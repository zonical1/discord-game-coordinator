import parsecsv
import region

normalserverlist = []
balanceserverlist = []

class Server:
    ServerName = None
    ServerIP = None
    ServerPort = None
    ServerMap = None
    ServerPlayers = None
    ServerRegion = None
    ServerRunning = None
    ServerID = None
    ServerGameMode = None
    IsBalanceServer = None

    def __init__(self, id : int, ip : str, port : int, region : region.BaseRegion , balance : bool):
        self.ServerIP = ip
        self.ServerPort = port
        self.ServerRegion = region
        self.ServerID = id
        self.IsBalanceServer = balance

    def __str__(self):
        return f"server[ServerName:{self.ServerName}, ServerRegion:{self.ServerRegion}, ServerIP:{self.ServerIP}, ServerPort:{self.ServerPort}, ServerMap:{self.ServerMap}, ServerPlayers:{self.ServerPlayers}, ServerRunning:{self.ServerRunning}, ServerID:{self.ServerID}, ServerGameMode:{self.ServerGameMode}, IsBalanceServer:{self.IsBalanceServer}]\n"

def InitServers():
    tempserverlist = parsecsv.ParseServerList("server-list.txt")
    
    for x in tempserverlist:
        print("{}, {}, {}".format(*x))

        NewRegion = region.CalculateRegion(x[3])

        x[3] = NewRegion
        x[1].strip('\n')

        if (x[4] == "True"):
            x[4] == True
            server = Server(x[0], x[1], x[2], x[3], x[4])
            balanceserverlist.append(server)
        elif (x[4] == "False"):
            x[4] == False
            server = Server(x[0], x[1], x[2], x[3], x[4])
            normalserverlist.append(server)

    for x in normalserverlist:
        print("Server: IP: {}, Port: {}, Region: {}, ID: {}".format(x.ServerIP, x.ServerPort, x.ServerRegion, x.ServerID))
    for x in balanceserverlist:
        print("Server: IP: {}, Port: {}, Region: {}, ID: {}".format(x.ServerIP, x.ServerPort, x.ServerRegion, x.ServerID))
