# discord-game-coordinator
A Discord bot that gets server information and determines the best server for the player to join.

Requires the following to run:
- python 3+
- python-valve
- console
- discord.py

You can edit server-list.txt to include the servers that you want, using the following format:
- Server ID (e.g 1), Server Address (e.g 127.0.0.1), Server Port (e.g 27015), Server Region (e.g usa, europe).
- Copy this: id,127.0.0.1,27015,region

The following regions work:
```python
usa, europe, aus, russia, brazil, singapore
```

When attempting to run the bot, paste your bot token ID into the empty "runtoken" string.
Then type !play (region) (gamemode). These following gamemodes work:
```python
payload, cp, koth, any, <blank> (same as any)
```

The bot is restricted to one channel, and it'll ping the original person who sent the message with the server information.

The bot is run from Lobbying.py. Make sure your terminal is able to use colors.
