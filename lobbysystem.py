#lobbyfile
import string
import random
import asyncio
import re
import sys
from region import CalculateRegion

lobbies = []
lobbycount = 0

gamemodes = {
    "payload": 1,
    "pl": 1,
    
    "cp": 2,

    "koth": 3,

    "ctf": 4,

    "any": 0,
    "all": 0,
}
    
def CalculateGamemode(string):
    if string in gamemodes:
        print("yes")
        return gamemodes.get(string)
    else:
        return None

class LobbyError(Exception):
    pass

class Lobby:
    PlayerID = None
    PlayerRegion = None
    PlayerContext = None
    PlayerGameMode = None
    WantsBalanceServer = None

    def __init__(self, context, region : str, gamemode):

        #We WANT a region, as long as it's not None.
        if region == None:
            raise LobbyError("Region is None.")
            return
        
        region = region.lower()
        region.strip('\n')
        region.strip(' ')

        #To prevent a None.lower() doesn't exist, we check the type of the gamemode.
        if gamemode != None:
            gamemode = gamemode.lower()

        '''So story time from ZoNiCaL, I really don't like this solution, so you can kinda call this spaghet, but whatever:
        #This part of the function is where I want to see if there is a +b in the region string, then slice the last two
        #characters off as it's where it's supposed to be.
        #Turns out that if I put the +b at the end of the string, it works.
        #If I put +b anywhere else in the string, it works but it slices off the last two characters like normal, giving
        #an invalid region to CalculateGamemode(region).

        #So... it works? But not the way I intended, hopefully I can make a much cleaner solution later.
        # 
        #25/04/2020 8:54AM - Fuckyes, found such an easier method lmao. '''
        
        #Check if we want to play on a balance server.
        if region.endswith('+b'):
            self.WantsBalanceServer = True
            #Remove the +b which is two characters long.
            region = region[:-2]
        else:
            self.WantsBalanceServer = False

        #Calculate the int value of the region and gamemode.
        self.PlayerRegion = CalculateRegion(region)
        self.PlayerGameMode = CalculateGamemode(gamemode)

        #If the lobby region is None or the lobby gamemode is None,
        if self.PlayerRegion == None or self.PlayerGameMode == None:
            raise LobbyError("PlayerRegion or PlayerGameMode is None.")
            return

        #Set the player ID and context.
        self.PlayerID = context.author
        self.PlayerContext = context

def CreateLobby(PlayerContext, PlayerRegion, PlayerGameMode):
    print(PlayerRegion, PlayerGameMode)
    try:
        lobby = Lobby(PlayerContext, PlayerRegion, PlayerGameMode)
        lobbies.append(lobby)
        return
    except LobbyError:
        print("An error has occoured in making a lobby")
        raise LobbyError
        return