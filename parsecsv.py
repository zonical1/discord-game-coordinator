import csv

servers = []
temp = []

def ParseServerList(file):
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:

                print(f'\tServer ID: {line_count}, Server Address: {row[0].lstrip()}, Server Port: {row[1]}, Server Region: {row[2].lstrip()}, Is Balance Mod: {row[3].lstrip()}')

                if row[0].startswith("//"):
                    continue
                
                temp.append(line_count) #ID
                temp.append(row[0].lstrip()) #IP
                temp.append(int(row[1])) #PORT
                temp.append(row[2].lstrip()) #REGION
                temp.append(row[3].lstrip()) #BALANCE MOD

                servers.append(temp.copy())
                temp.clear()

                line_count += 1
        print(f'Processed {line_count} lines.')

    return servers
