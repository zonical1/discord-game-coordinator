import lobbysystem as LobbyManager
import systemserver as ServerManager
import a2s
import discord
import asyncio
import sys
import region
from datetime import datetime

from discord.ext import commands, tasks
from console import fg, bg, fx
from console.screen import sc
from a_stdlib import Clear

bot = commands.Bot(command_prefix='c!', case_insensitive=True)
bot.remove_command("help")
runtoken = sys.argv[1]

channel = None
FirstTime = True

#FLAGS
debug = False
printStatistics = True

'''
Discord Game Coordinator

A bot used by Discord servers to provide a matchmaking service to players for Source Engine servers.
There are two main functions:

ServerLookLoop(): The loop that is responsible for updating the server register. Each server is stored in a Server class object which contains the following:
    - ServerName
    - ServerIP*
    - ServerPort*
    - ServerMap
    - ServerPlayers
    - ServerRegion*
    - ServerRunning
    - ServerID*
    - ServerGameMode
    - IsBalanceServer*
    *Assigned on startup, shouldn't be changed during runtime.

Each time the a server is pinged by A2S, it takes the following information out of server.info():
    - server_name (x.ServerName)
    - map (x.ServerMap)
    - player_count (x.ServerPlayers)

    If the server is successfully pinged, x.ServerRunning is set to True.
    The server's playercount is also added to the total playercount (TotalPlayerCount), and is transferred to oldbufferplayers when the loop restarts.

    ServerGameMode is set by taking the first two letters of ServerMap and checking them with CalculateGamemode(), returns None on failure.

If the server ping fails, the following things are set to None:
    - ServerMap
    - ServerPlayers
    - ServerName
    - ServerGameMode

    and ServerRunning is set to False.

This is all run from a seperate thread, controlled by the variable lookloop.

PlayerSortLoop(): The loop that is responsible for the player-server matchmaking. Each lobby/player is stored in a Lobby class object which contains the following:
    - PlayerID = None
    - PlayerRegion = None
    - PlayerContext = None
    - PlayerGameMode = None
    - WantsBalanceServer = None

When the player types !play <region> <gamemode> in #bots, the following variables are changed:
    - PlayerID = PlayerContext.author
    - PlayerRegion = <region>
    - PlayerContext = ctx
    - PlayerGameMode = <gamemode>

    If the player has a +b at the end of the region text, WantsBalanceServer is set to True, else, it's set to False.

Each time a lobby in LobbyManager.lobbies is checked, it loops thorugh all the servers, and a few checks are done:
    Before any servers are checked, bestserverplayers is set to -1 and is set to goodserver = None
    - If s.ServerRegion is not the same as i.PlayerRegion, the server is ignored.
    - If s.ServerGameMode is not the same as i.PlayerGameMode, the server is ignored.
    - If s.ServerRegion is the same as i.PlayerRegion, and s.serverPlayer is equal to 24 players/the server is full, the server is ignored.
    - If s.ServerRegion is the same as i.PlayerRegion, and s.ServerPlayers is equal to None, the server is ignored.
    - If s.ServerRegion is the same as i.PlayerRegion, and s.ServerPlayers is less than 24 and not None, and it's playercount is greater than bestserverplayers,
    it becomes the new goodserver and it's set to s (The server class object.).

If no goodserver is found, the lobby is skipped and it moves on to the next lobby in the LobbyManager.lobbies.
After checking all the servers, goodserver is deemed the best server to send to the player and the following is done:
    - goodserver is pinged by A2S one last time to see if the playercount has changed from the last time it was pinged in ServerLookLoop() and now.
        - If goodserver.ServerPlayers is equal to 24 players/the server is full, the server is ignored, and the lobby is skipped.
    
    - If the server check is good, an embeded message is constructed consisting of the server information, a Steam connect link, and a console connect link.
    - If the message was sent successfully, the lobby is popped from the list and the Lobby class object is deleted.

This is all run from a seperate thread, controlled by the variable playerloop.
'''

class ServerSearchError(BaseException):
    pass

def CalculateGamemode(string):
    if string.startswith("pl_"):
        return 1
    if string.startswith("cp_"):
        return 2
    if string.startswith("koth_"):
        return 3
    #Room for expandability:
    if string.startswith("ctf_"):
        return 4
    if string.startswith("plr_"):
        return 5
    if string.startswith("workshop/"):
        return 9

    raise ServerSearchError("Failed to calculate gamemode.")
    return None

TotalPlayerCount = 0
LastPlayerCount = 0

def GetServerInformation(server, printlocation):
    x = server
    global TotalPlayerCount

    try:
        #This is how the servers are pinged with A2S.
        server = a2s.info((x.ServerIP, x.ServerPort), timeout = 2)

        if server:
            #Storing the server information in the Server class object.
            x.ServerName = server.server_name
            x.ServerMap = server.map_name
            x.ServerPlayers = server.player_count
            x.ServerRunning = True

            #Statistics.
            x.ServerRegion.RegionPlayerCount += x.ServerPlayers
            x.ServerRegion.ConnectedServers += 1

            #Calculate the Gamemode value of the server.
            x.ServerGameMode = CalculateGamemode(x.ServerMap)

            TotalPlayerCount += x.ServerPlayers

            status = ""

            #Print to the terminal the status of the server.
            if (x.ServerPlayers == 24):
                x.ServerRegion.FullServers += 1
                status = "FULL"
                colorcode = fg.lightyellow
            else:
                status = "GOOD"
                colorcode = fg.lightgreen

            if debug == False:
                with sc.location(0, printlocation):
                    print(colorcode + f"\033[K[SERVER SEARCH - {status}]" + fg.white + f" Server: {x.ServerName}, IP: {x.ServerIP}:{x.ServerPort}, Map: {x.ServerMap}", fg.yellow, f"Players: {x.ServerPlayers}/24,", fg.white, 
                    f"Server Active: {x.ServerRunning}, Server Region: {x.ServerRegion}, Server Gamemode: {x.ServerGameMode}")
        else:
            raise ServerSearchError("Failed to set server object.")

    except Exception as e:
        #Printing to the terminal the status of the server.
        if debug == False:
            with sc.location(0, printlocation):
                print(fg.lightred + f"\033[K[SERVER SEARCH - FAIL] Could not connect to server. Error: {e}." + fg.white)

        x.ServerName = None
        x.ServerMap = None
        x.ServerPlayers = None
        x.ServerRunning = False
        x.ServerGameMode = None
        x.ServerRegion.FailServers += 1

FirstStartedAt = datetime.now().strftime("%H:%M:%S")

#This is the main loop that updates the server information.
@tasks.loop(seconds=0.1)
async def ServerLookLoop():
    FirstTime = False

    global TotalPlayerCount
    global LastPlayerCount
    global FirstStartedAt
    
    printloc = 1
    bufferplayers = 0

    #---------------------------------------------------------------------------------------------------------------
    #Printing at the top of the terminal the total player and server count.
    with sc.location(0, 0):
        print(fg.green, "\033[KAll systems operational... Logged in as {}, Started at: {}".format(bot.user.name, FirstStartedAt), fg.white)

     #We loop through each of the servers here.
    for x in ServerManager.normalserverlist:
        printloc += 1
        GetServerInformation(x, printloc)
        await asyncio.sleep(0.01)

    for x in ServerManager.balanceserverlist:
        printloc += 1
        GetServerInformation(x, printloc)
        await asyncio.sleep(0.01)

    #Statistics
    LastPlayerCount = TotalPlayerCount
    TotalPlayerCount = 0
    TotalConnectedServers = 0
    TotalFullServers = 0
    TotalFailServers = 0

    #---------------------------------------------------------------------------------------------------------------

    if debug == False:
        if printStatistics == True:
            printloc += 1

            #Get the current time.
            now = datetime.now()
            current_time = now.strftime("%H:%M:%S")

            with sc.location(0, printloc):
                print(fg.lightyellow, f"\033[K-------------------------------[REGION STATISTICS: {current_time}]-------------------------------")

            #Print the region playercounts at the bottom of the terminal.
            for x in region.regionlist:
                printloc += 1

                #TotalConnectedServers is the servers that we managed to ping successfully.
                #TotalFullServers is the servers that we managed to ping successfully and that were full.
                TotalConnectedServers += x.ConnectedServers
                TotalFullServers += x.FullServers

                #Print each regions total playercount.
                with sc.location(0, printloc):
                    print(fg.lightgreen, "\033[KRegion: ", fg.white, f"{x.RegionName}, Players: {x.RegionPlayerCount}/{24*x.ConnectedServers}, Connected Servers: {x.ConnectedServers}, Full Servers: {x.FullServers}")
                x.RegionPlayerCount = 0
                x.ConnectedServers = 0
                x.FullServers = 0
                x.FailServers = 0
                
            #Calculate the maximum amount of players.
            maxplayers = 24*TotalConnectedServers

            tpc_str = ""
            #Determine the colorcode for the total players print.
            if LastPlayerCount > maxplayers / 2:
                colorcode = fg.lightyellow
                tpc_str = " (Half Capacity)"
            elif LastPlayerCount > maxplayers - (maxplayers / 4):
                colorcode = fg.lightorange
                tpc_str = " (Almost Full Capacity)"
            elif LastPlayerCount == maxplayers:
                colorcode = fg.lightred
                tpc_str = " (Full Capacity)"
            else:
                tpc_str = " (Less than Half Capacity)"
                colorcode = fg.green

            printloc += 1
            #Print the total playercount across all regions.
            with sc.location(0, printloc):
                print(colorcode, "\033[KTotal Players: ", fg.white, f"{LastPlayerCount}/{maxplayers}", colorcode + tpc_str)

            #Print the server statistics.
            TotalFailServers = (len(ServerManager.balanceserverlist) + len(ServerManager.normalserverlist)) - TotalConnectedServers

            printloc += 1
            with sc.location(0, printloc):
                print(fg.lightyellow, f"\033[K-------------------------------[SERVER STATISTICS: {current_time}]-------------------------------\n"
                +fg.lightgreen, f"\033[KTotal Connected Servers: {TotalConnectedServers}\n"
                +fg.lightyellow, f"\033[KTotal Full Servers: {TotalFullServers}\n"
                +fg.lightred, f"\033[KTotal Failed Servers: {TotalFailServers}")
            
    await asyncio.sleep(1)

@ServerLookLoop.before_loop
async def ServerLookLoop_Before():
    await bot.wait_until_ready()

#This is the background loop that handles the soritng of players.
@tasks.loop(seconds=0.1)
async def PlayerSortLoop():
    good = False
    global channel

    #We do, so lets loop through them.
    for i in LobbyManager.lobbies[:]:
        #Initalize these two variables so that nothing screws up.
        bestserverplayers = -1
        goodserver = None

        #Lets loop through the servers and perform some checks.
        if i.WantsBalanceServer == False:
            searchlist = ServerManager.normalserverlist
        else:
            searchlist = ServerManager.balanceserverlist
        
        await asyncio.sleep(0.01)

        for s in searchlist:
            #If the server region is None for some reason, ignore it.
            if s.ServerRegion == None:
                continue
                    
            #This is kind of a hacky solution to allowing "any" or blank gamemode arguments.
            if i.PlayerGameMode == 0:
                FillInGamemode = s.ServerGameMode
            else:
                FillInGamemode = i.PlayerGameMode

            #If the server region to player region and server gamemode to player gamemode matches or the player gamemode is any:
            if debug == True:
                print("sr {} pr {} sg {} pg {}".format(s.ServerRegion, i.PlayerRegion, s.ServerGameMode, FillInGamemode))

            if s.ServerRegion == i.PlayerRegion and s.ServerGameMode == FillInGamemode:
                #Full?
                if s.ServerPlayers == 24:
                    continue

                if s.ServerPlayers == None:
                    continue
                        
                #We've passed all the checks, now if the playercount is greater than the bestserverplayers,
                #set the new bestserverplayers and goodserver.
                if s.ServerPlayers > bestserverplayers:
                    bestserverplayers = s.ServerPlayers
                    goodserver = s
                    continue
            else:
                continue
        
        #---------------------------------------------------------------------------------------------------------------
        if goodserver:
                #Lets do one final server check:
            try:
                server = a2s.info((goodserver.ServerIP, goodserver.ServerPort), timeout = 2)

                if server:
                    goodserver.ServerPlayers = server.player_count
                    goodserver.ServerMap = server.map_name
                    mapNameForThumbnail = goodserver.ServerMap
                else:
                    raise ServerSearchError("Failed to perform final check for PlayerSortLoop() -> goodserver")

                #The server's full. Shit.
                if goodserver.ServerPlayers == 24:
                    continue
            except:
                continue

            #Construct our embed message.
            sembed=discord.Embed(title=f"{goodserver.ServerName}", description=f"Click to Connect: steam://connect/{goodserver.ServerIP}:{goodserver.ServerPort}", color=0x5dff00)
            sembed.set_thumbnail(url=f"https://creators.tf/api/mapthumb?map={mapNameForThumbnail}")
            sembed.add_field(name="Map:", value="{}".format(goodserver.ServerMap), inline=True)
            sembed.add_field(name="Region:", value="{}".format(goodserver.ServerRegion.RegionEmoji), inline=True)
            sembed.add_field(name="Player Count:", value="{}/24".format(goodserver.ServerPlayers), inline=True)
            sembed.set_footer(text="Game coordinator created by ZoNiCaL#9740\n(https://github.com/zonical/discord-game-coordinator).")

            try:
                await channel.send("<@{}>, the Game Coordinator has found you a match!".format(i.PlayerID.id),embed=sembed)
                await i.PlayerContext.message.add_reaction('✅')
            except Exception as e:
                print("Failed to send message. {}".format(e))
                continue
                
            LobbyManager.lobbies.remove(i)
            await asyncio.sleep(1)

@PlayerSortLoop.before_loop
async def PlayerSortLoop_Before():
    await bot.wait_until_ready()

@bot.command()
@commands.cooldown(1, 10, commands.BucketType.user)
async def play(ctx, region = None, gamemode = None):
    global channel

    #Is the channel #bots?
    if ctx.message.channel != channel:
        return

    if gamemode == None:
        gamemode = "any"

    #Try and create the lobby.
    try:
        print("Creating lobby...")

        LobbyManager.CreateLobby(ctx, region, gamemode)
    except Exception as e:
        print("Failed to create lobby {}".format(e))
        await ctx.message.add_reaction('❌')
        await channel.send("{}, unfortunately, we failed to create a lobby for you. Please check that you have specificed a valid region `(e.g usa, europe)` and a valid gamemode `(e.g payload, any)`.\nOnce you've done this, retype `c!play <region> <gamemode>` in this channel!".format(ctx.author.name))
        return

    await channel.send("{}, you have been entered into the matchmaking queue.".format(ctx.author.name))

@bot.command()
async def regioninfo(ctx, lookregion = None):
    global channel

    if ctx.message.channel != channel:
        return

    if lookregion == None:
        await channel.send("<@{}>, please input a region to use with this command.")
        return
    
    regionlookup = region.CalculateRegion(lookregion)

    if regionlookup == None:
        await channel.send("<@{}>, please input a valid region to use with this command.")
        return

    #Create some new statistics:
    totalservers = regionlookup.ConnectedServers + regionlookup.FailServers

    #Server percentages.
    serverpercentage = ((regionlookup.ConnectedServers + regionlookup.FailServers) / len(ServerManager.balanceserverlist) + len(ServerManager.normalserverlist))
    onlineserverpercentage = (regionlookup.ConnectedServers / totalservers * 100)
    offlineserverpercentage = (regionlookup.FailServers / totalservers * 100)
    fullserverpercentage = (regionlookup.FullServers / totalservers * 100)

    #Player percentages.
    global TotalPlayerCount
    playercount_globalpercentage = (regionlookup.RegionPlayerCount / TotalPlayerCount * 100)
    playercount_regionpercentage = (regionlookup.RegionPlayerCount / (regionlookup.ConnectedServers * 24) * 100)

    #Construct the embed.
    sembed=discord.Embed(title="Creators.TF Game Coordinator", description="c!regioninfo", color=0xffda00)
    sembed.add_field(name="Server Count:", value=f"{totalservers}", inline=True)
    sembed.add_field(name="Server Makeup:", value=f"{serverpercentage:.2f}%", inline=True)
    sembed.add_field(name="Online Servers:", value=f"{regionlookup.ConnectedServers}/{totalservers} ({onlineserverpercentage:.2f}%)", inline=True)
    sembed.add_field(name="Offline Servers:", value=f"{regionlookup.FailServers}/{totalservers} ({offlineserverpercentage:.2f}%)", inline=True)
    sembed.add_field(name="Full Servers:", value=f"{regionlookup.FullServers}/{totalservers} ({fullserverpercentage:.2f}%)", inline=True)
    sembed.add_field(name="Current region playercount:", value=f"{regionlookup.RegionPlayerCount}/{totalservers*24} ({playercount_regionpercentage:.2f}%)", inline=True)
    sembed.add_field(name="Global playercount percentage:", value=f"{regionlookup.RegionPlayerCount}/{TotalPlayerCount} ({playercount_globalpercentage:.2f}%)", inline=True)
    sembed.set_footer(text="Game coordinator created by ZoNiCaL.")

    await channel.send(f"<@{ctx.author.id}>", embed=sembed)

@bot.command()
async def regions(ctx):
    global channel

    #Is the channel #bots?
    if ctx.message.channel != channel:
        return

    #Construct the embed.
    sembed=discord.Embed(title="Creators.TF Game Coordinator", description="c!regions", color=0xffda00)
    sembed.add_field(name="USA/North America:", value="usa, na, america, us", inline=False)
    sembed.add_field(name="Europe:", value="europe, eur, westeurope, eu", inline=False)
    sembed.add_field(name="Australia:", value="aus, aussie", inline=False)
    sembed.add_field(name="Brazil/South America:", value="brazil, sa", inline=False)
    sembed.add_field(name="Russia:", value="russia, westrussia, rus, ru", inline=False)
    sembed.add_field(name="Singapore/Asia:", value="singapore, asia", inline=False)
    sembed.set_footer(text="Game coordinator created by ZoNiCaL.")

    #Send a message both in the #bot channel and in the logging channel.
    await channel.send("<@{}>, these are the following regions and their aliases that you can use.".format(ctx.author.id), embed=sembed)

@bot.command()
async def gamemodes(ctx):
    global channel

    #Is the channel #bots?
    if ctx.message.channel != channel:
        return

    #Construct the embed.
    sembed=discord.Embed(title="Creators.TF Game Coordinator", description="c!gamemodes", color=0xffda00)
    sembed.add_field(name="Payload", value="payload, pl", inline=False)
    sembed.add_field(name="Control Points:", value="cp", inline=False)
    sembed.add_field(name="KOTH:", value="koth", inline=False)
    sembed.add_field(name="Any gamemode", value="any, all", inline=False)
    sembed.set_footer(text="Game coordinator created by ZoNiCaL.")

    #Send a message both in the #bot channel.
    await channel.send("<@{}>, these are the following gamemodes and their aliases that you can use.".format(ctx.author.id), embed=sembed)

@bot.command()
async def help(ctx):
    global channel

    #Is the channel #bots?
    if ctx.message.channel != channel:
        return

    #Construct the embed.
    sembed=discord.Embed(title="Creators.TF Game Coordinator", description="c!help", color=0xffda00)
    sembed.add_field(name="c!play <region> <gamemode>", value="Creates a lobby to search for an offical Creators.TF server using the bot.", inline=False)
    sembed.add_field(name="c!regions", value="Displays all of the valid regions that you can use in c!play", inline=False)
    sembed.add_field(name="c!gamemodes", value="Displays all of the valid gamemodes that you can use in c!play", inline=False)
    sembed.set_footer(text="Game coordinator created by ZoNiCaL.")

    #Send a message both in the #bot channel.
    await channel.send("<@{}>, here is the list of commands for the Discord Game Coordinator.".format(ctx.author.id), embed=sembed)

#Stop searching for lobbies.
@bot.command()
@commands.cooldown(1, 10, commands.BucketType.user)
async def stop(ctx):
    #Go through the lobbies.
    for x in LobbyManager.lobbies:
        if x.PlayerID == ctx.author:
            LobbyManager.lobbies.remove(x)
            await channel.send("<@{}>, a lobby of yours has been removed from the Game Coordinator.".format(ctx.author.id))
            await ctx.message.add_reaction('✅')
            return
    
    #Couldn't find one...
    await channel.send("<@{}>, we were unable to find a lobby that you owned.".format(ctx.author.id))
    await ctx.message.add_reaction('❌')

#Setup our bot, get our servers.
def InitBot():
    Clear()
    ServerManager.InitServers()
    try:
        bot.run(runtoken)
    except Exception as e:
        pass
    raise SystemExit

@play.error
async def play_error(ctx, error):
    global channel
    if isinstance(error, commands.CommandOnCooldown):
        await channel.send("<@{}>, you are on cooldown. Try again in {} seconds.".format(ctx.author.id, int(error.retry_after)))
        await ctx.message.add_reaction('⏰')

@stop.error
async def stop_error(ctx, error):
    global channel
    if isinstance(error, commands.CommandOnCooldown):
        await channel.send("<@{}>, you are on cooldown. Try again in {} seconds.".format(ctx.author.id, int(error.retry_after)))
        await ctx.message.add_reaction('⏰')

@bot.event
async def on_ready():
    #This is some simple bot logging, mainly used for debugging purposes.
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    print("\nStarted Discord Game Coordinator...\n")

    activity = discord.Activity(name=f'over {len(ServerManager.normalserverlist) + len(ServerManager.balanceserverlist)} servers.', type=discord.ActivityType.watching)
    await bot.change_presence(activity=activity)

    ServerLookLoop.start()
    PlayerSortLoop.start()

    #Set our channel for message checking.
    global channel
    
    #channel = bot.get_channel(646814033097261056)
    channel = bot.get_channel(700751235346727033)
    Clear()

@bot.event
async def on_disconnect():
    print("Disconnected from Discord Game Coordinator...\n")
    await bot.logout()

@bot.event
async def on_error(event, *args, **kwargs):
    pass
    #print(f"Discord ran into an error!")
    
if (__name__ == '__main__'):
    InitBot()
